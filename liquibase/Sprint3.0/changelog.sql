--liquibase formatted sql

--changeset amit:7
create table if not exists policy(
    pol_id int primary key,
    address1 varchar(50),
    address2 varchar(50),
    city varchar(30)
)
--rollback drop table policy

--changeset amit:8
create table if not exists premium(
    prm_trx_id int primary key,
   pol_id int,
    amount int
)
--rollback drop table premium

--changeset amit:9
insert into policy values (1, 'Test Policy', 'Newport Associates', '52 East Street', 'Wellington');
insert into policy values (2, 'Test Policy 2', 'ABS Const', '33 Downtown', 'New York');
--rollback delete from policy where pol_id=1;
--rollback delete from policy where pol_id=2;

--changeset amit:10
insert into policy values (1, 'Test Policy', 'Newport Associates', '52 East Street', 'Wellington');
